import { Component, Input, OnInit } from '@angular/core';
import { Recipe } from '../recipe.model';
import { RecipeService } from '../recipe.service';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.scss'],
})
export class RecipeDetailComponent implements OnInit {
  @Input()
  selectedRecipe!: Recipe;
  constructor(private recipeService: RecipeService) {}
  onSLAdd() {
    this.recipeService.addToSL(this.selectedRecipe.ingredients);
  }
  ngOnInit(): void {}
}
