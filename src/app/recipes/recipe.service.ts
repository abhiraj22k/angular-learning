import { EventEmitter, Injectable } from '@angular/core';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Recipe } from './recipe.model';

@Injectable()
export class RecipeService {
  recipeSelected = new EventEmitter<Recipe>();
  constructor(private shoppingListService: ShoppingListService) {}
  private recipes: Recipe[] = [
    new Recipe(
      'Pav Bhaji',
      'Super tasty and spicy Pav Bhaji',
      'https://www.cookwithmanali.com/wp-content/uploads/2018/05/Best-Pav-Bhaji-Recipe.jpg',
      [new Ingredient('Pav', 100), new Ingredient('Bhaji', 150)]
    ),
    new Recipe(
      'Sev Puri',
      'Special Sev Puri',
      'https://shwetainthekitchen.com/wp-content/uploads/2021/10/sev-puri.jpg',
      [new Ingredient('Sev', 60), new Ingredient('Puri', 30)]
    ),
  ];
  getRecipes() {
    return this.recipes.slice();
  }
  addRecipes(recipe: Recipe) {
    this.recipes.push(recipe);
  }
  addToSL(ingredients: Ingredient[]) {
    this.shoppingListService.addIngredients(ingredients);
  }
}
